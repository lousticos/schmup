﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BulletType
{
    Player,
    EnemyBullet,
    WaveBullet
}

public abstract class Bullet : MonoBehaviour
{
    protected Vector2 speed;
    protected float damage;

    public bool used = false;


    protected BulletType bullettype;

    public BulletType Bullettype
    {
        get
        {
            return this.bullettype;
        }
        private set
        {
            this.bullettype = value;
        }
    }

    public float Damage
    {
        get
        {
            return this.damage;
        }
        private set
        {
            this.damage = value;
        }
    }

    public Vector2 Speed
    {
        get
        {
            return this.speed;
        }
        private set
        {
            this.speed = value;
        }
    }

    public Vector2 Position
    {
        get
        {
            return gameObject.transform.position;
        }
        private set
        {
            gameObject.transform.position = value;
        }
    }

    public void Init(BulletType nbullettype)
    {
        bullettype = nbullettype;
    }

    public void Init(float ndamage, Vector2 nspeed, Vector2 nposition)
    {
        damage = ndamage;
        speed = nspeed;
        gameObject.transform.position = nposition;
    }

    public void Init(float ndamage,Vector2 nspeed,Vector2 nposition,BulletType nbullettype)
    {
        damage = ndamage;
        speed = nspeed;
        gameObject.transform.position = nposition;
        bullettype = nbullettype;
    }

    protected virtual void UpdatePosition()
    {
        gameObject.transform.position += (Vector3) speed * Time.deltaTime;
    }

    private void FixedUpdate()
    {
        UpdatePosition();
    }

    private void OnBecameInvisible()
    {
        BulletFactory.Instance.Release(this);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if((bullettype == BulletType.Player && collision.tag == "enemy") || (bullettype == BulletType.EnemyBullet && collision.tag == "player"))
        {
            collision.gameObject.GetComponent<BaseAvatar>().TakeDamages(damage);
            BulletFactory.Instance.Release(this);
        }
    }
}
