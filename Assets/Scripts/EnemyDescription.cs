﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

public class EnemyDescription
{
    public float spawnPosition;
    public float spawnDate;
    public int enemyType;

    [XmlIgnore]
    public bool spawned = false;

    /*public void enemydescription()
    {
        spawnPosition = 0;
        spawnDate = 0;
        enemyType = 0;
    }*/
}
