﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemySingleton<EnemyFactory> : MonoBehaviour where EnemyFactory : MonoBehaviour
{
    // Check to see if we're about to be destroyed.
    private static bool m_ShuttingDown = false;
    private static object m_Lock = new object();
    private static EnemyFactory m_Instance;

    /// <summary>
    /// Access singleton instance through this propriety.
    /// </summary>
    public static EnemyFactory Instance
    {
        get
        {
            if (m_ShuttingDown)
            {
                Debug.LogWarning("[Singleton] Instance '" + typeof(EnemyFactory) +
                    "' already destroyed. Returning null.");
                return m_Instance;
            }

            lock (m_Lock)
            {
                if (m_Instance == null)
                {
                    // Search for existing instance.
                    m_Instance = (EnemyFactory)FindObjectOfType(typeof(EnemyFactory));

                    // Create new instance if one doesn't already exist.
                    if (m_Instance == null)
                    {
                        // Need to create a new GameObject to attach the singleton to.
                        var singletonObject = new GameObject();
                        m_Instance = singletonObject.AddComponent<EnemyFactory>();
                        singletonObject.name = typeof(EnemyFactory).ToString() + " (Singleton)";

                        // Make instance persistent.
                        DontDestroyOnLoad(singletonObject);
                    }
                }

                return m_Instance;
            }
        }
    }

    private void OnApplicationQuit()
    {
        m_ShuttingDown = true;
    }


    private void OnDestroy()
    {
        m_ShuttingDown = true;
    }
}

public class EnemyFactory : Singleton<EnemyFactory>
{
    [SerializeField]
    private GameObject enemy1;
    [SerializeField]
    private GameObject enemy2;

    [SerializeField]
    private List<Engine> enemy1reserve = new List<Engine>();
    private List<AIEnemyBasicEngine2> enemy2reserve = new List<AIEnemyBasicEngine2>();

    [SerializeField]
    private int reservesize = 25;

    private void Start()
    {

        for (int j = 0; j < 2; j++)
        {
            for (int i = 0; i < reservesize; i++)
            {
                if (j == 0)
                {
                    GameObject enemy = Instantiate(enemy1);
                    enemy.SetActive(false);
                    enemy.transform.position = new Vector3(-100, -100, 0);
                    enemy1reserve.Add(enemy.GetComponent<Engine>());
                }
                else
                {
                    GameObject enemy = Instantiate(enemy2);
                    enemy.SetActive(false);
                    enemy.transform.position = new Vector3(-100, -100, 0);
                    enemy2reserve.Add(enemy.GetComponent<AIEnemyBasicEngine2>());
                }
            }
        }

    }

    public GameObject GetEnemy(int type)
    {
        if (type == 1)
        {
            for (int i = 0; i < enemy1reserve.Count; i++)
            {
                if (!enemy1reserve[i].used)
                {
                    enemy1reserve[i].gameObject.SetActive(true);
                    enemy1reserve[i].used = true;
                    return enemy1reserve[i].gameObject;
                }
            }
            GameObject enemy = Instantiate(enemy1);
            Engine engine = enemy.GetComponent<Engine>();
            enemy1reserve.Add(engine);
            engine.used = true;
            return engine.gameObject;
        }
        else
        {
            for (int i = 0; i < enemy2reserve.Count; i++)
            {
                if (!enemy2reserve[i].used)
                {
                    enemy2reserve[i].gameObject.SetActive(true);
                    enemy2reserve[i].used = true;
                    return enemy2reserve[i].gameObject;
                }
            }
            GameObject enemy = Instantiate(enemy2);
            AIEnemyBasicEngine2 engine = enemy.GetComponent<AIEnemyBasicEngine2>();
            enemy2reserve.Add(engine);
            engine.used = true;
            return engine.gameObject;
        }

    }

    public void Release(Engine engine)
    {
        engine.gameObject.transform.position = new Vector3(-100, -100, 0);
        engine.used = false;
        engine.gameObject.SetActive(false);
    }

    public void Release(AIEnemyBasicEngine2 engine)
    {
        engine.gameObject.transform.position = new Vector3(-100, -100, 0);
        engine.used = false;
        engine.gameObject.SetActive(false);
    }
}