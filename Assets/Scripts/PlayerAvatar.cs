﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAvatar : BaseAvatar
{
    protected int maxenergy = 100;
    protected int energy = 100;
    protected int requiredenergy = 10;
    protected bool discharged = false;
    protected float count = 0;


    public static event DeathEvent OnPlayerDeath;

    public int MaxEnergy
    {
        get
        {
            return this.maxenergy;
        }
        private set
        {
            this.maxenergy = value;
        }
    }

    public void FixedUpdate()
    {
        if (energy < maxenergy )
        {
            count += Time.deltaTime;
            if(count >= 1)
            {
                if (!discharged)
                    energy += Mathf.FloorToInt(10 * count);
                else
                    energy += Mathf.FloorToInt(7.5f * count);
                if (energy > maxenergy)
                    energy = maxenergy;
                if (energy == maxenergy)
                    discharged = false;
                count = 0;
            }

        }
        else
            discharged = false;

    }

    public float EnergyFloatValue()
    {
        /*if (discharged)
            return energy + 7.5f * count;
        return energy + 10 * count;*/
        return energy;
    }

    public bool EnergyCheck()
    {
        return energy >= requiredenergy && !discharged;
    }

    public void LoseEnergy()
    {
        energy -= requiredenergy;
        if (energy < 0)
            energy = 0;
        if (energy == 0)
            discharged = true;
    }

    protected override void Die()
    {
        if(OnPlayerDeath != null)
        {
            OnPlayerDeath();
        }
        base.Die();
    }
}
