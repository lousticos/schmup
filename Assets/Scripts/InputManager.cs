﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public Engine engine;
    public BulletGun bulletgun;
    public BulletGunDiag bulletgundiag;
    public GameObject bulletgundiagobject;
    public BulletGun bulletgunwave;
    public GameObject bulletgunwaveobject;
    private int guntype = 0;

    private bool saveUp = false;
    private bool saveDown = false;
    private bool saveLeft = false;
    private bool saveRight = false;

    private float timer = 0;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }

        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");
        bool fireOn = Input.GetKey(KeyCode.Space);
        bool swapweapon = Input.GetKeyDown(KeyCode.Tab);
        bool up = Input.GetKeyDown(KeyCode.UpArrow);
        bool down = Input.GetKeyDown(KeyCode.DownArrow);
        bool left = Input.GetKeyDown(KeyCode.LeftArrow);
        bool right = Input.GetKeyDown(KeyCode.RightArrow);
        if (swapweapon)
        {
            if (guntype == 0)
            {
                guntype = 1;
                bulletgundiagobject.GetComponent<SpriteRenderer>().enabled = true;
            }
            else if (guntype == 1)
            {
                guntype = 2;
                bulletgundiagobject.GetComponent<SpriteRenderer>().enabled = false;
                bulletgunwaveobject.GetComponent<SpriteRenderer>().enabled = true;
            }
            else if (guntype == 2)
            {
                guntype = 0;
                bulletgunwaveobject.GetComponent<SpriteRenderer>().enabled = false;
            }
        }
        if (fireOn)
        {
            if (guntype == 0)
                bulletgun.Fire();
            if (guntype == 1)
                bulletgundiag.Fire();
            if(guntype == 2)
                bulletgunwave.Fire();
        }


        if(up || down || right || left)
        {
            if(up != saveUp || down != saveDown || left != saveLeft || right != saveRight)
            {
                saveUp = up;
                saveDown = down;
                saveLeft = left;
                saveRight = right;
                timer = 0.40f;
            }
            else
            {
                if(timer > 0)
                {
                    engine.Dash(new Vector2(10 * moveHorizontal, 10 * moveVertical));
                    timer = 0;
                    saveUp = false;
                    saveDown = false;
                    saveLeft = false;
                    saveRight = false;
}
                else
                {
                    saveUp = up;
                    saveDown = down;
                    saveLeft = left;
                    saveRight = right;
                }
            }
        }

        //Debug.Log(saveHorizontal + "    " + saveVertical);*/

        engine.speed = 2 * (new Vector2(moveHorizontal, moveVertical));
    }

    
}
