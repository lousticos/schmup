﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBasicBulletGun : MonoBehaviour
{
    [SerializeField]
    private float damage;
    [SerializeField]
    private Vector2 speed;
    [SerializeField]
    private float cooldown;
    [SerializeField]
    private Bullet bullet;
    private float count = -1;

    public float Damage
    {
        get
        {
            return this.damage;
        }
        private set
        {
            this.damage = value;
        }
    }

    public Vector2 Speed
    {
        get
        {
            return this.speed;
        }
        private set
        {
            this.speed = value;
        }
    }

    public float Cooldown
    {
        get
        {
            return cooldown;
        }
        private set
        {
            cooldown = value;
        }
    }

    private void FixedUpdate()
    {
        if (count > 0)
            count -= Time.deltaTime;
        Fire();
    }

    public void Fire()
    {
        
        if (count < 0)
        {
            Bullet newbullet = BulletFactory.Instance.GetBullet(BulletType.EnemyBullet);
            newbullet.Init(damage, speed, gameObject.transform.position, BulletType.EnemyBullet);
            count = cooldown;
        }
    }
}
