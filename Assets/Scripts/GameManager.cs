﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class GameManager : MonoBehaviour
{
    private GameObject player;
    [SerializeField]
    private GameObject playerprefab;
    [SerializeField]
    private GameObject enemyprefab;
    private float count = 0;
    public UIManager uimanager;

    [SerializeField]
    private GameObject[] enemyprefabs;

    public List<LevelDescription> data = new List<LevelDescription>();

    private Level level = new Level();

    private void Start()
    {
        LoadLevel("level.xml");
        level.Load(data[0], Time.time, this);
        player = Instantiate(playerprefab, new Vector3(-7, 0, 0), Quaternion.identity);
        uimanager.playeravatar = player.GetComponent<PlayerAvatar>();
    }

    
    void OnEnable()
    {
        PlayerAvatar.OnPlayerDeath += ToMenu;
    }


    void OnDisable()
    {
        PlayerAvatar.OnPlayerDeath += ToMenu;
    }
    

    // Update is called once per frame
    void Update()
    {
        /*
        if (count > 1)
        {
            float y = Random.Range(-4.8f, 4.8f);
            Instantiate(enemyprefab, new Vector3(10, y, 0), Quaternion.identity);
            count = 0;
        }
        count += Time.deltaTime;*/
        level.Update();
    }

    void ToMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    void LoadLevel(string filename)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(LevelDescription));
        FileStream fs = new FileStream(filename, FileMode.Open);
        LevelDescription level = (LevelDescription)serializer.Deserialize(fs);
        data.Add(level);
        Debug.Log("name : " + level.name + " enemy : " + level.enemyDescriptions.ToString());
    }

    void SaveLevel(LevelDescription level, string filename, int id)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(LevelDescription));
        TextWriter writer = new StreamWriter(filename);
        serializer.Serialize(writer, data[id]);
        writer.Close();
    }

    public void Spawn(int enemyid, float enemyposition)
    {
        GameObject enemy =  EnemyFactory.Instance.GetEnemy(enemyid);
        enemy.transform.position = new Vector3(10, enemyposition, 0);
        //Instantiate(enemyprefabs[enemyid],new Vector3(10,enemyposition,0),Quaternion.identity);
    }
}
