﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEnemyBasicEngine2 : MonoBehaviour
{

    private Vector2 speed = new Vector2 (-1,0);
    //Rigidbody2D newrigidbody;
    //public float tilt;
    [SerializeField]
    private Boundary boundary;

    private float ypos;

    public bool used = false;

    private void OnEnable()
    {
        ypos = gameObject.transform.position.y;
    }

    private void Start()
    {
        ypos = gameObject.transform.position.y;
    }

    void FixedUpdate()
    {
        gameObject.transform.position += (Vector3)speed * Time.deltaTime;
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, ypos + Mathf.Sin(gameObject.transform.position.x), 0);

        //rigidbody.rotation = Quaternion.Euler(0.0f, 0.0f, rigidbody.velocity.x * -tilt);
    }

    private void OnBecameInvisible()
    {
        //Destroy(gameObject);
        EnemyFactory.Instance.Release(this);
    }
}
