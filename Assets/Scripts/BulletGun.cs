﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletGun : MonoBehaviour
{
    [SerializeField]
    private float damage;
    [SerializeField]
    private Vector2 speed;
    [SerializeField]
    private float cooldown;
    [SerializeField]
    private BulletType type = BulletType.Player;
    private float count = -1;
    [SerializeField]
    private PlayerAvatar playeravatar;



    public float Damage
    {
        get
        {
            return this.damage;
        }
        private set
        {
            this.damage = value;
        }
    }

    public Vector2 Speed
    {
        get
        {
            return this.speed;
        }
        private set
        {
            this.speed = value;
        }
    }

    public float Cooldown
    {
        get
        {
            return cooldown;
        }
        private set
        {
            cooldown = value;
        }
    }

    private void FixedUpdate()
    {
        if (count > 0)
            count -= Time.deltaTime;
    }

    public void Fire()
    {
        if (count < 0)
        {
            if (playeravatar.EnergyCheck())
            {
                Bullet newbullet = BulletFactory.Instance.GetBullet(type);
                newbullet.Init(damage, speed, gameObject.transform.position);
                count = cooldown;
                playeravatar.LoseEnergy();
            }

        }
    }

}
