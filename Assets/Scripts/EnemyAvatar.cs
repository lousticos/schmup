﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAvatar : BaseAvatar
{
    protected int score = 50;

    public static event DeathEvent OnEnemyDeath;

    protected override void Die()
    {
        if (OnEnemyDeath != null)
        {
            OnEnemyDeath();
        }
        base.Die();
    }
}
