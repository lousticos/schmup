﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Slider energyslider;
    public Slider healthslider;
    public PlayerAvatar playeravatar;
    public Text scoretext;

    private int score = 0;

    void OnEnable()
    {
        score = 0;
        EnemyAvatar.OnEnemyDeath += AddScore;
    }


    void OnDisable()
    {
        EnemyAvatar.OnEnemyDeath += AddScore;
    }

    private void FixedUpdate()
    {
        if(playeravatar != null)
        {
            energyslider.value = playeravatar.EnergyFloatValue();
            energyslider.maxValue = playeravatar.MaxEnergy;
            healthslider.value = playeravatar.Health;

            healthslider.maxValue = playeravatar.MaxHealth;
        }
    }

    private void AddScore()
    {
        score += 50;
        scoretext.text = "Score = " + score;
    }
}
