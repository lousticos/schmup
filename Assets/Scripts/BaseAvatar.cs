﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseAvatar : MonoBehaviour
{
    [SerializeField]
    protected float maxhealth;
    [SerializeField]
    protected float health;

    protected bool invulnerable = false;

    public delegate void DeathEvent();

    public float MaxHealth
    {
        get
        {
            return this.maxhealth;
        }
        private set
        {
            this.maxhealth = value;
        }
    }

    public float Health
    {
        get
        {
            return this.health;
        }
        private set
        {
            this.health = value;
        }
    }

    public void TakeDamages(float damages)
    {
        if (!invulnerable)
        {
            health = health - damages;
            if (health <= 0)
                Die();
        }

    }

    protected virtual void Die()
    {
        Destroy(gameObject);
    }

    public void SetInvulnerable(bool newinvulnerable)
    {
        invulnerable = newinvulnerable;
    }
}
