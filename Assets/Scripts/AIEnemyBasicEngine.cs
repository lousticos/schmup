﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEnemyBasicEngine : MonoBehaviour
{
    public Engine engine;

    private void Start()
    {
        engine.speed = new Vector2(-1, 0);
    }

    private void OnBecameInvisible()
    {
        //Destroy(gameObject);
        EnemyFactory.Instance.Release(engine);
    }
}
