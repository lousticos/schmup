﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveBullet : Bullet
{
    [SerializeField]
    private GameObject bullet;
    private Vector3 move;
    private Vector3 origin;

    private void Start()
    {
        move = gameObject.transform.localPosition;
        origin = move;
    }

    protected override void UpdatePosition()
    {
        move += (Vector3)speed * Time.deltaTime;
        gameObject.transform.localPosition = new Vector3((Mathf.Cos(move.x - origin.x)-1) + move.x, Mathf.Sin(move.x - origin.x) + move.y);
    }
}
