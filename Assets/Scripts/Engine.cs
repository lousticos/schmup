﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class Engine : MonoBehaviour
{
    public Vector2 speed;
    //Rigidbody2D newrigidbody;
    //public float tilt;
    [SerializeField]
    private Boundary boundary;

    private Vector2 dashspeed;
    private bool isdashing;
    private float dashtimer;

    private BaseAvatar avatar;

    public bool used;

    /*private void Start()
    {
        newrigidbody = gameObject.GetComponent<Rigidbody2D>();
    }*/

    private void Start()
    {
        avatar = gameObject.GetComponent<BaseAvatar>();
    }

    void FixedUpdate()
    {

        if (isdashing)
        {
            gameObject.transform.position += (Vector3)dashspeed * Time.deltaTime;
            dashtimer -= Time.deltaTime;
            if (dashtimer < 0)
            {
                isdashing = false;
                avatar.SetInvulnerable(false);
            }
        }
        else
        {
            gameObject.transform.position += (Vector3)speed * Time.deltaTime;
        }


        gameObject.transform.position = new Vector3
        (
            Mathf.Clamp(gameObject.transform.position.x, boundary.xMin, boundary.xMax),
            Mathf.Clamp(gameObject.transform.position.y, boundary.zMin, boundary.zMax),
            0.0f
        );
        //rigidbody.rotation = Quaternion.Euler(0.0f, 0.0f, rigidbody.velocity.x * -tilt);
    }

    public void Dash(Vector2 direction)
    {
        dashspeed = direction;
        dashtimer = 0.25f;
        isdashing = true;
        avatar.SetInvulnerable(true);
    }

}
