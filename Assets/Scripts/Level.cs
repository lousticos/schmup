﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level
{
    private LevelDescription definition;
    private float startingtime;
    GameManager gamemanager;

    public void Load(LevelDescription newdefinition, float newstartingtime, GameManager newgamemanager)
    {
        definition = newdefinition;
        startingtime = newstartingtime;
        gamemanager = newgamemanager;
    }

    public void Update()
    {
        for (int i = 0; i < definition.enemyDescriptions.Length; i++)
        {
            if(definition.enemyDescriptions[i].spawnDate <= Time.time - startingtime && !definition.enemyDescriptions[i].spawned)
            {
                gamemanager.Spawn(definition.enemyDescriptions[i].enemyType, definition.enemyDescriptions[i].spawnPosition);
                definition.enemyDescriptions[i].spawned = true;
            }
        }
    }
}
