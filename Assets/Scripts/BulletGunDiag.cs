﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletGunDiag : MonoBehaviour
{
    [SerializeField]
    private float damage = 5;
    [SerializeField]
    private Vector2 speed ;
    [SerializeField]
    private float cooldown = 0.2f;
    [SerializeField]
    private Bullet bullet;
    private float count = -1;
    [SerializeField]
    private PlayerAvatar playeravatar;


    public float Damage
    {
        get
        {
            return this.damage;
        }
        private set
        {
            this.damage = value;
        }
    }

    public Vector2 Speed
    {
        get
        {
            return this.speed;
        }
        private set
        {
            this.speed = value;
        }
    }

    public float Cooldown
    {
        get
        {
            return cooldown;
        }
        private set
        {
            cooldown = value;
        }
    }

    private void FixedUpdate()
    {
        if (count > 0)
            count -= Time.deltaTime;
    }

    public void Fire()
    {
        if (count < 0)
        {
            if (playeravatar.EnergyCheck())
            {
                Bullet newbullet1 = BulletFactory.Instance.GetBullet(BulletType.Player);
                Bullet newbullet2 = BulletFactory.Instance.GetBullet(BulletType.Player);
                newbullet1.transform.Rotate(0, 0, 45);
                newbullet2.transform.Rotate(0, 0, -45);
                Vector2 speed1 = new Vector2(3, 3);
                Vector2 speed2 = new Vector2(3, -3);
                newbullet1.Init(damage, speed1, gameObject.transform.position, BulletType.Player);
                newbullet2.Init(damage, speed2, gameObject.transform.position, BulletType.Player);
                count = cooldown;
                playeravatar.LoseEnergy();
            }

        }
    }
}
