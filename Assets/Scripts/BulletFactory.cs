﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<BulletFactory> : MonoBehaviour where BulletFactory : MonoBehaviour
{
    // Check to see if we're about to be destroyed.
    private static bool m_ShuttingDown = false;
    private static object m_Lock = new object();
    private static BulletFactory m_Instance;

    /// <summary>
    /// Access singleton instance through this propriety.
    /// </summary>
    public static BulletFactory Instance
    {
        get
        {
            if (m_ShuttingDown)
            {
                Debug.LogWarning("[Singleton] Instance '" + typeof(BulletFactory) +
                    "' already destroyed. Returning null.");
                return m_Instance;
            }

            lock (m_Lock)
            {
                if (m_Instance == null)
                {
                    // Search for existing instance.
                    m_Instance = (BulletFactory)FindObjectOfType(typeof(BulletFactory));

                    // Create new instance if one doesn't already exist.
                    if (m_Instance == null)
                    {
                        // Need to create a new GameObject to attach the singleton to.
                        var singletonObject = new GameObject();
                        m_Instance = singletonObject.AddComponent<BulletFactory>();
                        singletonObject.name = typeof(BulletFactory).ToString() + " (Singleton)";

                        // Make instance persistent.
                        DontDestroyOnLoad(singletonObject);
                    }
                }

                return m_Instance;
            }
        }
    }

    private void OnApplicationQuit()
    {
        m_ShuttingDown = true;
    }


    private void OnDestroy()
    {
        m_ShuttingDown = true;
    }
}

public class BulletFactory : Singleton<BulletFactory>
{
    [SerializeField]
    private GameObject playerbullet;
    [SerializeField]
    private GameObject wavebullet;
    [SerializeField]
    private GameObject enemybullet;

    [SerializeField]
    private List<Bullet> playerbulletreserve = new List<Bullet>();
    private List<Bullet> wavebulletreserve = new List<Bullet>();
    private List<Bullet> enemybulletreserve = new List<Bullet>();

    [SerializeField]
    private int reservesize = 25;

    private void Start()
    {

        for(int j = 0; j < 3; j++)
        {
            for (int i = 0; i < reservesize; i++)
            {
                if(j == 0)
                {
                    GameObject bulletobject = Instantiate(playerbullet);
                    Bullet bullet = bulletobject.GetComponent<Bullet>();
                    bullet.Init(BulletType.Player);
                    bullet.gameObject.transform.position = new Vector3(-100, -100, 0);
                    //bullet.enabled = false;
                    bullet.gameObject.SetActive(false);
                    playerbulletreserve.Add(bullet);
                }else if(j == 1)
                {
                    GameObject bulletobject = Instantiate(wavebullet);
                    Bullet bullet = bulletobject.GetComponent<Bullet>();
                    bullet.Init(BulletType.Player);
                    bullet.gameObject.transform.position = new Vector3(-100, -100, 0);
                    //bullet.enabled = false;
                    bullet.gameObject.SetActive(false);
                    wavebulletreserve.Add(bullet);
                }
                else
                {
                    GameObject bulletobject = Instantiate(enemybullet);
                    Bullet bullet = bulletobject.GetComponent<Bullet>();
                    bullet.Init(BulletType.EnemyBullet);
                    bullet.gameObject.transform.position = new Vector3(-100, -100, 0);
                    //bullet.enabled = false;
                    bullet.gameObject.SetActive(false);
                    enemybulletreserve.Add(bullet);
                }
            }
        }

    }

    public Bullet GetBullet(BulletType type)
    {
        if (type == BulletType.WaveBullet)
        {
            for (int i = 0; i < wavebulletreserve.Count; i++)
            {
                if (!wavebulletreserve[i].used)
                {
                    //wavebulletreserve[i].enabled = true;
                    wavebulletreserve[i].gameObject.SetActive(true);
                    wavebulletreserve[i].used = true;
                    return wavebulletreserve[i];
                }
            }
            GameObject bulletobject = Instantiate(wavebullet);
            Bullet bullet = bulletobject.GetComponent<Bullet>();
            bullet.Init(BulletType.Player);
            wavebulletreserve.Add(bullet);
            bullet.used = true;
            return bullet;
        }
        else if (type == BulletType.Player)
        {
            for (int i = 0; i < playerbulletreserve.Count; i++)
            {
                if (!playerbulletreserve[i].used)
                {
                    //playerbulletreserve[i].enabled = true;
                    playerbulletreserve[i].gameObject.SetActive(true);
                    playerbulletreserve[i].used = true;
                    return playerbulletreserve[i];
                }
            }
            GameObject bulletobject = Instantiate(playerbullet);
            Bullet bullet = bulletobject.GetComponent<Bullet>();
            bullet.Init(BulletType.Player);
            playerbulletreserve.Add(bullet);
            bullet.used = true;
            return bullet;
        }
        else
        {
            for (int i = 0; i < enemybulletreserve.Count; i++)
            {
                if (!enemybulletreserve[i].used)
                {
                    //enemybulletreserve[i].enabled = true;
                    enemybulletreserve[i].gameObject.SetActive(true);
                    enemybulletreserve[i].used = true;
                    return enemybulletreserve[i];
                }
            }
            GameObject bulletobject = Instantiate(enemybullet);
            Bullet bullet = bulletobject.GetComponent<Bullet>();
            bullet.Init(BulletType.EnemyBullet);
            enemybulletreserve.Add(bullet);
            bullet.used = true;
            return bullet;
        }

    }

    public void Release(Bullet bullet)
    {
        bullet.gameObject.transform.position = new Vector3(-100, -100, 0);
        bullet.used = false;
        //bullet.enabled = false;
        bullet.gameObject.SetActive(false);
    }
}
